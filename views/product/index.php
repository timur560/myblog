<?php
/**
 * @var app\models\Product[] $products
 */

?>

<h1>Products</h1>

<?php foreach ($products as $product) : ?>

<div class="col-md-3">
    <p><?=$product->title?></p>
    <p>$<?=$product->price?></p>
    <a href="/cart/add-product?id=<?=$product->id?>" class="btn btn-success">Buy</a>
</div>

<?php endforeach ?>
