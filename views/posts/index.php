<?php
/**
 * @var \app\models\Post[] $posts
 */

?>
<h1><?=$title?></h1>

<?php if (\Yii::$app->session->hasFlash('message')) : ?>
    <div class="alert alert-danger">
        <?=\Yii::$app->session->getFlash('message')?>
    </div>
<?php endif ?>

<p>
    <a href="/posts/create" class="btn btn-primary">Create post</a>
</p>
<?php foreach ($posts as $post) : ?>

<div class="col-md-4">
    <h2><?=$post->title?></h2>
    <p><?=$post->short?></p>
    <?=\yii\helpers\Html::a(
            'View',
            '/posts/view/' . $post->id,
            ['class' => 'btn btn-primary'])
    ?>

</div>

<?php endforeach; ?>
