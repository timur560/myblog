<?php
/**
 * @var \app\models\Post $post
 * @var \app\models\Comment $newComment
 */

use yii\widgets\ActiveForm;

?>
<h1><?=$post->title?></h1>
<p><?=$post->description?></p>

<h2>Comments</h2>
<?php foreach ($post->comments as $comment) : ?>
    <p><b>User: </b><?=$comment->name?></p>
    <p><?=$comment->content ?></p>
    <p><b>Post: </b><?=$comment->post->title?></p>
    <br>
<?php endforeach ?>

<h2>Add comment</h2>
<?php $form = ActiveForm::begin(['action' => '/posts/add-comment']) ?>
    <?=$form
        ->field($newComment, 'postId')
        ->hiddenInput()
        ->label(false)
    ?>
    <?=$form->field($newComment, 'name'); ?>
    <?=$form
        ->field($newComment, 'content')
        ->textarea(); ?>
    <button type="submit" class="btn btn-success">Add</button>
<?php ActiveForm::end()?>
