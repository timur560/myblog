<h1>Edit Post</h1>

<?php use yii\widgets\ActiveForm;

$form = ActiveForm::begin(); ?>

    <?= $form->field($post, 'title') ?>
    <?= $form->field($post, 'short')
        ->label('Short text')
        ->hint('Maximum 200 symbol')
    ?>
    <?= $form->field($post, 'description')
        ->textarea(['rows' => 10])
    ?>

    <div class="form-group">
        <button type="submit" class="btn btn-primary">Save</button>
        <a href="/posts/index" class="btn btn-danger">Cancel</a>
    </div>
<!--</form>-->
<?php ActiveForm::end() ?>