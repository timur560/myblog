<h1>Cart</h1>

<table class="table">
    <?php foreach ($cart as $item) : ?>
    <tr>
        <td><?=$item['product']->title?></td>
        <td>$<?=$item['product']->price?></td>
        <td><?=$item['count']?></td>
    </tr>
    <?php endforeach ?>
</table>

<p><b>Total:</b> $<?=\Yii::$app->cart->getTotal()?></p>

<a href="/cart/create-order" class="btn btn-success">Create Order</a>
