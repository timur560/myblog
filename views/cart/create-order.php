<h1>Create Order</h1>
<?php $form = \yii\widgets\ActiveForm::begin() ?>
    <?=$form->field($order, 'address')->textarea()?>
    <?=$form->field($order, 'comment')->textarea()?>
    <button class="btn btn-success">Create!</button>
<?php \yii\widgets\ActiveForm::end() ?>
