<?php

use yii\db\Migration;

/**
 * Class m190818_143659_create_store_tables
 */
class m190818_143659_create_store_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('order', [
            'id' => $this->primaryKey(),
            'address' => $this->text(),
            'comment' => $this->string(1000),
            'userId' => $this->integer(),
        ]);

        $this->addForeignKey(
            'fk_order_user',
            'order',
            'userId',
            'user',
            'id'
        );

        $this->createTable('product', [
            'id' => $this->primaryKey(),
            'title' => $this->string(200),
            'description' => $this->text(),
            'price' => $this->decimal(6, 2),
        ]);

        $this->insert('product', [
            'title' => 'MacBook',
            'description' => 'Best notebook ever',
            'price' => 2000,
        ]);

        $this->insert('product', [
            'title' => 'Thinkpad',
            'description' => 'Best notebook ever 2',
            'price' => 1000,
        ]);

        $this->createTable('order_product', [
            'id' => $this->primaryKey(),
            'orderId' => $this->integer(),
            'productId' => $this->integer(),
            'count' => $this->integer()->defaultValue(1),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190818_143659_create_store_tables cannot be reverted.\n";

        return false;
    }

}
