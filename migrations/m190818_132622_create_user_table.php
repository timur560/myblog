<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%user}}`.
 */
class m190818_132622_create_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(100),
            'email' => $this->string(200),
            'password' => $this->string(200),
            'authKey' => $this->string(),
            'accessToken' => $this->string(),
        ]);

        $this->insert('user', [
            'name' => 'John Doe',
            'email' => 'johndoe@gmail.com',
            'password' => Yii::$app->security->generatePasswordHash('123456'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%user}}');
    }
}
