<?php

use yii\db\Migration;

/**
 * Class m190811_134442_create_comments
 */
class m190811_134442_create_comments extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('comment', [
            'id' => $this->primaryKey(),
            'name' => $this->string(50),
            'content' => $this->text(),
            'createdAt' => $this->dateTime(),
            'postId' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('comment');
    }

}
