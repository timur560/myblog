<?php

use yii\db\Migration;

/**
 * Class m190811_135835_add_test_comment
 */
class m190811_135835_add_test_comment extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn(
            'comment',
            'createdAt',
            $this->dateTime()->defaultExpression('NOW()')
        );

        $this->insert('comment', [
            'name' => 'John Doe',
            'content' => 'Awesome post!',
            'postId' => 1
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190811_135835_add_test_comment cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190811_135835_add_test_comment cannot be reverted.\n";

        return false;
    }
    */
}
