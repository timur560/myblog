<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "post".
 *
 * @property int $id
 * @property string $title
 * @property string $short
 * @property string $description
 *
 * @property Comment[] $comments
 */
class Post extends \yii\db\ActiveRecord
{
    const SCENARIO_ADMIN_EDIT = 'admin_edit';
    const SCENARIO_USER_EDIT = 'user_edit';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'post';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['description'], 'string'],
            ['title', 'string', 'max' => 50, 'min' => 5],
            ['title', 'default', 'value' => 'Default title'],
            ['short', 'string', 'max' => 1000],
            [
                ['title', 'short', 'description'],
                'required',
                'on' => [self::SCENARIO_USER_EDIT],
                'message' => 'Field cannot be empty',
            ],
            [
                ['title', 'description'],
                'required',
                'on' => [self::SCENARIO_ADMIN_EDIT],
                'message' => 'Field cannot be empty',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'short' => 'Short',
            'description' => 'Description',
        ];
    }

    public function scenarios()
    {
        return array_merge(
            parent::scenarios(),
            [
                self::SCENARIO_ADMIN_EDIT => ['title', 'short', 'description'],
                self::SCENARIO_USER_EDIT => ['title', 'short', 'description'],
            ]
        );
    }

    public function getComments()
    {
        return $this->hasMany(
            Comment::class,
            ['postId' => 'id']
        );
    }
}
