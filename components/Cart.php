<?php


namespace app\components;


use app\models\Product;
use yii\base\Component;

class Cart extends Component
{
    public function addProduct($id)
    {
        $cart = \Yii::$app->session->get('cart', []);

        if (isset($cart[$id])) {
            $cart[$id]++;
        } else {
            $cart[$id] = 1;
        }

        \Yii::$app->session->set('cart', $cart);
    }

    public function getProducts()
    {
        $result = [];

        $cart = \Yii::$app->session->get('cart', []);

        foreach ($cart as $id => $count) {
            $result[] = [
                'product' => Product::findOne($id),
                'count' => $count,
            ];
        }

        return $result;
    }

    public function getTotal()
    {
        $result = 0;
        $cart = \Yii::$app->session->get('cart', []);

        foreach ($cart as $id => $count) {
            $result += Product::findOne($id)->price * $count;
        }

        return $result;
    }
}