<?php
/**
 * Created by PhpStorm.
 * User: oculus
 * Date: 14.08.19
 * Time: 20:22
 */

namespace app\components;

use yii\filters\AccessControl;
use yii\web\Controller;

class AdminController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['index', 'view', 'update', 'delete'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        'roles' => ['?'], // '?' - guest, '@' - user
                    ],
                    [
                        'allow' => false,
                        'actions' => ['view', 'delete', 'update'],
                        'roles' => ['?'], // '?' - guest, '@' - user
                    ],
                ],
            ],
        ];
    }

//    public function beforeAction($action)
//    {
//        if (\Yii::$app->user->isGuest) {
//            return $this->redirect('/site/login');
//        }
//
//        return parent::beforeAction($action);
//    }

}