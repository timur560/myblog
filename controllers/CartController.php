<?php


namespace app\controllers;


use app\models\Order;
use yii\filters\AccessControl;
use yii\web\Controller;

class CartController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['create-order'],
                'rules' => [
                    [
                        'allow' => false,
                        'actions' => ['create-order'],
                        'roles' => ['?'], // '?' - guest, '@' - user
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create-order'],
                        'roles' => ['@'], // '?' - guest, '@' - user
                    ],
                ],
            ],
        ];
    }

    public function actionAddProduct($id)
    {
        \Yii::$app->cart->addProduct($id);
        $this->redirect('/product');
    }

    public function actionIndex()
    {
        $cart = \Yii::$app->cart->getProducts();
        return $this->render('index', ['cart' => $cart]);
    }

    public function actionCreateOrder()
    {
        $order = new Order();
        if (\Yii::$app->request->isPost) {
            $order->load(\Yii::$app->request->post());
            $order->userId = \Yii::$app->user->id;
            $order->save();

            $products = \Yii::$app->cart->getProducts();

            foreach ($products as $product) {
                $order->link('products', $product['product'], ['count' => $product['count']]);
            }

        }

        return $this->render('create-order', ['order' => $order]);
    }
}