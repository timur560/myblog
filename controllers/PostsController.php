<?php
/**
 * Created by PhpStorm.
 * User: oculus
 * Date: 31.07.19
 * Time: 19:27
 */

namespace app\controllers;


use app\models\Comment;
use app\models\Post;
use yii\db\Query;
use yii\web\Controller;

class PostsController extends Controller
{
    public function actionIndex()
    {
//        $posts = \Yii::$app->db
//            ->createCommand('select * from `post`')
//          ->queryAll();
//          ->queryOne();
//          ->execute();

//        $posts = (new Query())
//            ->from('post')
//            ->select(['id', 'title', 'short'])
//            ->limit(5)
//            ->where(['id' => [2, 3]])
//            ->count();
//            ->all();
//            ->one()

        $posts = Post::find()->all();

//        var_dump($posts);die;

        return $this->render(
            'index',
            [
                'title' => 'Posts List',
                'posts' => $posts,
            ]
        );
    }

    public function actionView($id)
    {
//        $post = Post::find()->where(['id' => $id])->one();
        $post = Post::findOne($id);

        return $this->render('view', [
            'post' => $post,
            'newComment' => new Comment(['postId' => $id]),
        ]);
    }

    public function actionCreate()
    {
        $post = new Post();

        if (\Yii::$app->request->isPost) {

            // $post->setAttributes(\Yii::$app->request->post());

            // var_dump(\Yii::$app->request->post());die;

            $post->load(\Yii::$app->request->post());

//            $post->title = \Yii::$app->request->post('title');
//            $post->short = \Yii::$app->request->post('short');
//            $post->description = \Yii::$app->request->post('description');

            $post->setScenario(Post::SCENARIO_USER_EDIT);

            if ($post->save()) {
                $this->redirect('/posts/index');
            }
        }

        return $this->render('create', ['post' => $post]);
    }

    public function actionUpdate($id)
    {
        $post = Post::findOne($id);

        if (\Yii::$app->request->isPost) {

            $post->load(\Yii::$app->request->post());
            $post->setScenario(Post::SCENARIO_USER_EDIT);
            if ($post->save()) {
                $this->redirect('/posts/index');
            }
        }

        return $this->render('update', ['post' => $post]);
    }

    public function actionDelete($id)
    {
        $post = Post::findOne($id);
        $post->delete();
        \Yii::$app->session->setFlash('message', 'Post ID#' . $id . ' deleted');
        $this->redirect('/posts/index');
    }

    public function actionAddComment()
    {
        $comment = new Comment();
        $comment->load(\Yii::$app->request->post());
        $comment->save();
        $this->redirect('/posts/view/' . $comment->postId);
    }
}